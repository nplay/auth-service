const { ServiceBroker } = require("moleculer");
const ApiService = require("moleculer-web");
import {Config} from "core"

const broker = new ServiceBroker({
    nodeID: process.env.HOSTNAME,
    transporter: Config.queue.driver.rabbitMQ.connectionString
});

import AuthService from './authService'

let fnRun = async () => {

    // Create a service
    await broker.createService(AuthService);

    // Load API Gateway
    await broker.createService({
        mixins: [ApiService],

        settings: {
            port: 3004
        },
    });

    // Start server
    await broker.start();
}

fnRun()