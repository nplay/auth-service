
const mongoose = require('mongoose');
import {Config} from "core"

export default {
    name: "auth",
    version: 1,
    events: {
    },
    methods: {
        async conn() {
            let db = Config.database
            let connString = `mongodb://${db.host}:${db.port}/${db.db}`

            return mongoose.createConnection(connString, {
                poolSize: 5
            })
        },
        async byPublicKeys(keys){
            
            let conn = null

            try {
                conn = await this.conn()

                let collection = conn.db.collection('users')
                let result = await collection.findOne({
                    "stores.applications.keys.value": { $in: keys }
                }, {_id: 1})
                
                return result

            } finally {
                conn.close()
            }
        }
    },
    actions: {
        async validate(ctx) {

            let response = {
                body: {
                    success: true,
                    allowed: false,
                    tokenPayload: null
                }
            }
            
            if(!ctx.params.token)
                response.body.success = false
            else
            {
                let token = ctx.params.token

                let buff = Buffer.from(token.split('.')[1], 'base64');
                response.body.tokenPayload = buff.toString('ascii');
            }

            let exists = await this.byPublicKeys([ctx.params.token]) != null
            response.body.allowed = exists

            return response
        }
    }
}